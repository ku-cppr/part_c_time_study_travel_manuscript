## Manuscript Title:

Staff travel time in rural- and urban-serving Infants and Toddlers with Disabilities (Part C) programs in Kansas

## Description

Code is provided for review for those who wish to see how we calculated numbers.
Deidentified data is also provided for those who want to test out the code themselves; however, results will be different from the real data.
In folder "03_real_figures" you can find the figures used in the manuscript.
When the manuscript is published, we will update this README with a link to it.

## Authors and acknowledgment

Christopher D. Tilden		https://orcid.org/0000-0003-3949-5920 

Nathan C. Kuhn		https://orcid.org/0000-0003-0755-1203 

Silke A. von Esenwein	https://orcid.org/0000-0003-4716-3678 

Sushmita Samaddar		https://orcid.org/0000-0001-8401-1767 

Hilde A. McKee		https://orcid.org/0000-0001-8651-4712  

Rebecca J. Gillam		https://orcid.org/0000-0003-4409-2704 



University of Kansas Center for Public Partnerships and Research (KU-CPPR)
https://cppr.ku.edu/


We have no conflicts of interest to disclose.  

Correspondence regarding this article should be addressed to Chris Tilden, Ph.D., 1617 St. Andrews Drive, Lawrence, Kansas 66047. Email: ctilden@ku.edu.  

