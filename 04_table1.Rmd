---
title: "04_table1"
author: "Nathan Kuhn"
output: html_document
---

Purpose: Replicate Table 1.

If run in console:
rmarkdown::render("04_table1.Rmd", envir = globalenv())
--- Will created file "04_table1.Rmd.html" which you can then open in browser.

### Using texreg::htmlreg():
```{r, results = "asis"}
texreg::knitreg(
  l = list(
    lm_robust_ppl_popDn_m1_classical
    ,lm_robust_ppl_popDn_m2_clustered_CR2
  )
  ,stars = c(.1,.05,.01) 
  ,custom.model.names = c('Model 1: OLS', 'Model 2: OLS clustered SE')
  ,custom.coef.names = c("Intercept", "Log of Population Density (ppl. per sq. mi.)")
  ,custom.gof.names = c(
    'R^2' = NA,
    'Adj. R^2' = NA,
    'Num. obs.' = 'Observations',
    'N Clusters' = 'Clusters'
  )
  ,custom.gof.rows = list(
    'Intercept' = c(
      lm_robust_ppl_popDn_m1_classical$coefficients[['(Intercept)']]
      ,lm_robust_ppl_popDn_m2_clustered_CR2$coefficients[['(Intercept)']]
    )
    ,'Description of model' = c(
      'Ordinary least squares'
      ,'Ordinary least squares, clustered & robust standard errors'
    )
  )
  ,custom.note = '%stars \n\n Clustered by program; clustered robust standard errors using "CR2."'
  ,omit.coef = 'Intercept'
  ,reorder.gof = c(1, 5, 6, 3, 4, 2) ### With 2 custom.gof.rows.
  ,caption = NULL ### Removed default "Statistical models" caption.
  ,include.ci = FALSE
  ,include.rmse = FALSE
)
```




